const vid = document.getElementsByClassName('section__video')[0];
const play = document.getElementsByClassName('section__img--play')[0];

play.addEventListener("click", function (e) {
  vid.classList.add('section__video--is-active');
  play.classList.add('section__img--no-active');
});

const noplay = document.getElementsByTagName('video')[0];

noplay.addEventListener("click", function (e) {
   play.classList.remove('section__img--no-active');
   vid.classList.remove('section__video--is-active');
});