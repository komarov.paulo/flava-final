import slick from 'slick-carousel';
import $ from 'jquery';

 $('.cards-wrapper--lineup').slick({
   arrows: true,
   slidesToShow: 1,
   slidesToScroll: 1,
   mobileFirst: true,
   responsive: [
           {
             breakpoint: 767,
             settings: {
               slidesToShow: 2,
             }
           },
           {
            breakpoint: 1199,
            settings: {
              slidesToShow: 3,
            }
           },
         ],

 nextArrow: '<a class="control control--next" ><img class="control__img" src="../static/img/arrow_r.svg"/> </a>',
 prevArrow: '<a class="control control--prev" ><img class="control__img" src="../static/img/arrow_l.svg"/> </a>',
 } 
 );

 $('.cards-wrapper--sponsors').slick({
   arrows: false,
   dots: true,
   slidesToShow: 2,
   slidesToScroll: 2,
   mobileFirst: true,
   responsive: [
      {
        breakpoint: 767,
        settings: {
         slidesToShow: 3,
         slidesToScroll: 2,
              }
      },
      {
         breakpoint: 1199,
         settings: {
           slidesToShow: 6,
           slidesToScroll: 3,
         }
        },
    ],
 });
