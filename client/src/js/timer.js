let date = new Date('Sep 22 2021 00:00:00');

function nowDate() {
  let now = new Date();
  let gap = date - now;
  let days = Math.floor(gap / 1000 / 60 / 60 / 24);
  let hours = Math.floor(gap / 1000 / 60 / 60) % 24;
  let minutes = Math.floor(gap / 1000 / 60) % 60;
  let seconds = Math.floor(gap / 1000) % 60;

  if (gap < 0) {
    days = days + 30;
    hours = hours + 24;
    minutes = minutes + 60;
    seconds = seconds + 60;
  }

  document.getElementById('d').innerText = days;
  document.getElementById('h').innerText = hours;
  document.getElementById('m').innerText = minutes;
  document.getElementById('s').innerText = seconds;
}

nowDate();
setInterval(nowDate, 1000);